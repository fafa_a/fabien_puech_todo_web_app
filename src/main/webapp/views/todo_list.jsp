<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="header.jsp"></jsp:include>
<div class="row">
  <div class="container">
    <h3 class="text-center text-capitalize">${username}'s Todos</h3>
    <hr />
    <div class="container text-left">
      <a
        href="<%=request.getContextPath()%>/do?action=DisplayTodoForm"
        class="btn btn-success"
        >Add Todo</a
      >
    </div>
    <br />
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Title</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="todo" items="${listTodos}">
          <tr>
            <td><c:out value="${todo.title}" /></td>
            <td>
              <c:out value="${todo.completed ? 'Completed' : 'In progess' }" />
            </td>

            <td>
              <a
                href="do?action=DisplayTodoForm&id=<c:out value='${todo.idtodo}' />"
                >Edit</a
              >
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="do?action=DeleteTodo&id=<c:out value='${todo.idtodo}' />"
                >Delete</a
              >
            </td>
          </tr>
        </c:forEach>
        <!-- } -->
      </tbody>
    </table>
  </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>
