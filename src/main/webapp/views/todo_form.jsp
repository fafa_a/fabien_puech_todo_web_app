<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c"%>
<jsp:include page="header.jsp"></jsp:include>
<div class="container col-md-5">
  <div class="card">
   <div class="card-body">
    <c:if test="${todo != null}">
     <form action="<%= request.getContextPath()%>/do?action=UpdateTodo" method="post">
    </c:if>
    <c:if test="${todo == null}">
     <form action="<%= request.getContextPath()%>/do?action=CreateTodo" method="post">
    </c:if>
    <caption>
     <h2>
      <c:if test="${todo != null}">
               Edit Todo
              </c:if>
      <c:if test="${todo == null}">
               Add New Todo
              </c:if>
     </h2>
    </caption>

    <c:if test="${todo != null}">
     <input type="hidden" name="id" value="<c:out value='${todo.idtodo}' />" />
    </c:if>

    <fieldset class="form-group">
     <label>Todo Title</label> <input type="text"
      value="<c:out value='${todo.title}' />" class="form-control"
      name="title" required="required" minlength="5">
    </fieldset>

    <fieldset class="form-group">
     <label>Todo Description</label> <input type="text"
      value="<c:out value='${todo.description}' />" class="form-control"
      name="description" minlength="5">
    </fieldset>

    <fieldset class="form-group">
      <label>Todo Status</label>
      <select class="form-control" name="isDone"  >
       <option value="false" <c:if test="${!todo.completed}">selected=true</c:if>>In Progress</option>
       <option value="true" <c:if test="${todo.completed}">selected=true</c:if>>Complete</option>
      </select>
     </fieldset>

    <button type="submit" class="btn btn-success">Save</button>
    </form>
   </div>
  </div>
 </div>
 <jsp:include page="footer.jsp"></jsp:include>