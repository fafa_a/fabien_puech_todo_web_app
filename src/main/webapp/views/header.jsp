<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%> <%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="ISO-8859-1" />
    <title>Todo App</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />
  </head>
  <style>
    .row {
      margin-left: 0;
      margin-right: 0;
    }
    .footer {
      position: fixed;
      bottom: 0;
      width: 100%;
      height: 40px;
      background-color: #09ab6e;
    }
  </style>

  <body>
    <header>
      <nav
        class="navbar navbar-expand-md navbar-dark"
        style="background-color: #09ab6e"
      >
        <div>
          <a
            href="<%=request.getContextPath()%>/do?action=Login"
            class="navbar-brand"
          >
            Todo App</a
          >
        </div>

        <ul class="navbar-nav navbar-collapse justify-content-end">
          <c:if test="${username != null}">
            <li class="nav-item">
              <a
                class="nav-link"
                href="<%=request.getContextPath()%>/do?action=LogOut"
              >
                Logout
              </a>
            </li>
          </c:if>
        </ul>
      </nav>
    </header>
  </body>
</html>
