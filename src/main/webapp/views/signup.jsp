<%-- Created by IntelliJ IDEA. User: 31010-67-02 Date: 16/03/2022 Time: 10:24 To
change this template use File | Settings | File Templates. --%> <%@ page
contentType="text/html;charset=UTF-8" language="java" %> <%@ taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Insert title here</title>

    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />
  </head>

  <body>
    <jsp:include page="header.jsp"></jsp:include>
    <div class="container">
      <h2>User Register Form</h2>
      <div class="col-md-6 col-md-offset-3">
        <div class="alert alert-success center" role="alert">
          <p>${NOTIFICATION}</p>
        </div>

        <form
          action="<%=request.getContextPath()%>/do?action=ValidateSignup"
          method="post"
        >
          <div class="form-group">
            <label for="fname">First Name:</label
            ><input
              type="text"
              class="form-control"
              id="fname"
              placeholder="First Name"
              name="firstName"
              required
            />
          </div>
          <div class="form-group">
            <label for="lname">Last Name:</label
            ><input
              type="text"
              class="form-control"
              id="lname"
              placeholder="Last Name"
              name="lastName"
              required
            />
          </div>
          <div class="form-group">
            <label for="username">User Name:</label
            ><input
              type="text"
              class="form-control"
              id="username"
              placeholder="User Name"
              name="userName"
              required
            />
          </div>
          <div class="form-group">
            <label for="password">Password:</label
            ><input
              type="text"
              class="form-control"
              id="password"
              placeholder="Password"
              name="password"
              required
            />
          </div>

          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </body>
</html>
