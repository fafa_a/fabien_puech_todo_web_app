package fr.afpa.cda.services;

import fr.afpa.cda.models.LoginBean;
import fr.afpa.cda.models.UserBean;

public interface IUserService {

    public UserBean createUser(UserBean userBean);

    public UserBean validateLogin(LoginBean loginBean);

}
