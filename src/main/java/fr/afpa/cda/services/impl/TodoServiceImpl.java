package fr.afpa.cda.services.impl;

import java.util.List;

import fr.afpa.cda.dal.ITodoDao;
import fr.afpa.cda.dal.implJPA.TodoDaoJPAImpl;
import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.models.UserBean;
import fr.afpa.cda.models.mappers.TodoMapper;
import fr.afpa.cda.services.ITodoService;

public class TodoServiceImpl implements ITodoService {

    private ITodoDao todoDao;

    public TodoServiceImpl() {
	super();
	todoDao = new TodoDaoJPAImpl();
    }

    @Override
    public TodoBean createTodo(TodoBean todoBean) {
	// TodoEntity todoEntity = TodoMapper.toEntity(todoBean);
	// todoEntity = todoDao.create(todoEntity);
	// return TodoMapper.toBean(todoEntity);
	return TodoMapper.toBean(todoDao.create(TodoMapper.toEntity(todoBean)));
    }

    @Override
    public List<TodoBean> getTodosByUser(UserBean userBean) {
	return TodoMapper.toBeanList(todoDao.getAllTodosByUsername(userBean.getUsername()));
    }

    @Override
    public List<TodoBean> getTodosByUser(String username) {
	return TodoMapper.toBeanList(todoDao.getAllTodosByUsername(username));
    }

	@Override
	public void updateTodo(TodoBean todoBean) {
		
		 todoDao.update(TodoMapper.toEntity(todoBean));
		 
	}

	@Override
	public TodoBean findTodoById(int id) {
		
		return TodoMapper.toBean(todoDao.findById(id));
	}

	@Override
	public void deleteTodo(int id) {
		
		todoDao.delete(todoDao.findById(id));
		
	}

}
