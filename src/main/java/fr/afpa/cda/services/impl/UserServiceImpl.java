package fr.afpa.cda.services.impl;

import fr.afpa.cda.dal.IUserDao;
import fr.afpa.cda.dal.implJPA.UserDaoJPAImpl;
import fr.afpa.cda.models.LoginBean;
import fr.afpa.cda.models.UserBean;
import fr.afpa.cda.models.entities.UserEntity;
import fr.afpa.cda.models.mappers.UserMapper;
import fr.afpa.cda.services.IUserService;

public class UserServiceImpl implements IUserService {

  private IUserDao userDao;

  public UserServiceImpl() {
    super();
    userDao = new UserDaoJPAImpl();
  }

  @Override
  public UserBean createUser(UserBean userBean) {
    UserEntity userEntity = UserMapper.toEntity(userBean);
    userEntity = userDao.create(userEntity);
    return UserMapper.toBean(userEntity);
  }

  @Override
  public UserBean validateLogin(LoginBean loginBean) {
    // TODO Auto-generated method stub
    return null;
  }

  public boolean userExist(String username, String password) {
    return userDao.userExist(username, password);
  }
}
