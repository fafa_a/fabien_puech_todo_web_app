package fr.afpa.cda.services;

import java.util.List;

import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.models.UserBean;

public interface ITodoService {

    public TodoBean createTodo(TodoBean todoBean);

    public List<TodoBean> getTodosByUser(UserBean userBean);

    public List<TodoBean> getTodosByUser(String username);
    
    public void updateTodo(TodoBean todoBean);
    
    public TodoBean findTodoById (int id);
    
    public void deleteTodo(int id);

}