package fr.afpa.cda.dal;

import java.util.List;

import fr.afpa.cda.models.entities.TodoEntity;

public interface ITodoDao extends IDao<TodoEntity, Integer> {

    public List<TodoEntity> getAllTodosByUsername(String username);

}
