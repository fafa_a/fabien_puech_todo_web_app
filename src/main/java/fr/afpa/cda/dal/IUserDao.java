package fr.afpa.cda.dal;

import fr.afpa.cda.models.entities.UserEntity;

public interface IUserDao extends IDao<UserEntity, Integer> {

  public UserEntity getByUsername(String username);

  public boolean userExist(String username, String password);
}
