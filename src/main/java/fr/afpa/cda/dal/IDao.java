package fr.afpa.cda.dal;

import java.util.List;

public interface IDao<T, ID> {

    public T create(T t);

    public T update(T t);

    public T findById(ID id);

    public List<T> findAll();

    public void delete(T t);

    public void deleteById(ID id);

}
