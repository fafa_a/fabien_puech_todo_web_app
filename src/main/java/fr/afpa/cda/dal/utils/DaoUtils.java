package fr.afpa.cda.dal.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoUtils {

    private static EntityManagerFactory emf = null;

    public static synchronized EntityManagerFactory getEmf() {
	return emf == null ? Persistence.createEntityManagerFactory("TodoJPABDD") : emf;
    }

    public static synchronized void closeEmf() {
	if (emf != null) {
	    emf.close();
	    emf = null;
	}
    }

}
