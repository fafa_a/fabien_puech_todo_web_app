package fr.afpa.cda.dal.implJPA;

import javax.persistence.EntityManager;

import fr.afpa.cda.dal.IUserDao;
import fr.afpa.cda.models.entities.UserEntity;

public class UserDaoJPAImpl extends DaoJPAImpl<UserEntity, Integer> implements IUserDao {

  @Override
  public Class<UserEntity> getClazz() {
    return UserEntity.class;
  }

  @Override
  public UserEntity getByUsername(String name) {
    EntityManager em = createEntityManager();
    String jpql = "select u from " + UserEntity.class.getName() + " u where u.username = :p_uname";
    UserEntity userEntity = em.createQuery(jpql, UserEntity.class).setParameter("p_uname", name).getSingleResult();
    closeEntityManager(em);
    return userEntity;
  }

  @Override
  public boolean userExist(String username, String password) {
    EntityManager em = createEntityManager();
    Boolean userExist = false;
    String jpql = "select u from " + UserEntity.class.getName()
        + " u where u.username = :p_username and u.password = :p_password";
    UserEntity userEntity = em.createQuery(jpql, UserEntity.class).setParameter("p_username", username)
        .setParameter("p_password", password).getSingleResult();
    if (userEntity != null) {
      userExist = true;
    }
    closeEntityManager(em);
    return userExist;
  }
}