package fr.afpa.cda.dal.implJPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import fr.afpa.cda.dal.IDao;
import fr.afpa.cda.dal.utils.DaoUtils;

public abstract class DaoJPAImpl<T, ID> implements IDao<T, ID> {

  private Class<T> clazz = getClazz();

  public abstract Class<T> getClazz();

  @Override
  public T create(T t) {
    EntityManager em = createEntityManager();
    em.getTransaction().begin();
    em.persist(t);
    em.getTransaction().commit();
    closeEntityManager(em);
    return t;
  }

  @Override
  public T update(T t) {
    EntityManager em = createEntityManager();
    em.getTransaction().begin();
    t = em.merge(t);
    em.getTransaction().commit();
    closeEntityManager(em);
    return t;
  }

  @Override
  public T findById(ID id) {
    EntityManager em = createEntityManager();
    T t = em.find(clazz, id);
    closeEntityManager(em);
    return t;
  }

  @Override
  public List<T> findAll() {
    EntityManager em = createEntityManager();
    String jpql = "select t from " + clazz.getName() + " t";
    List<T> liste = em.createQuery(jpql, clazz).getResultList();
    closeEntityManager(em);
    return liste;
  }

  @Override
  public void delete(T t) {
    EntityManager em = createEntityManager();
    em.getTransaction().begin();
    em.remove(em.merge(t));
    em.getTransaction().commit();
    closeEntityManager(em);
  }

  @Override
  public void deleteById(ID id) {
    EntityManager em = createEntityManager();
    em.getTransaction().begin();
    em.remove(findById(id));
    em.getTransaction().commit();
    closeEntityManager(em);
  }

  ///////////////////////////////
  ///////////////////////////////

  protected EntityManager createEntityManager() {
    return DaoUtils.getEmf().createEntityManager();
  }

  protected void closeEntityManager(EntityManager em) {
    if (em != null && em.isOpen()) {
      EntityTransaction t = em.getTransaction();
      if (t.isActive()) {
        t.rollback();
      }
      em.close();
    }
  }

}
