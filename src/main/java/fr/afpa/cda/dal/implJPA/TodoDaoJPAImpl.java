package fr.afpa.cda.dal.implJPA;

import java.util.List;

import javax.persistence.EntityManager;

import fr.afpa.cda.dal.ITodoDao;
import fr.afpa.cda.models.entities.TodoEntity;

public class TodoDaoJPAImpl extends DaoJPAImpl<TodoEntity, Integer> implements ITodoDao {

    @Override
    public List<TodoEntity> getAllTodosByUsername(String username) {
	EntityManager em = createEntityManager();
	String jpql = "select t from " + TodoEntity.class.getName() + " t where t.username = :p_name";
	List<TodoEntity> liste = em.createQuery(jpql, TodoEntity.class).setParameter("p_name", username)
		.getResultList();
	closeEntityManager(em);
	return liste;
    }

    @Override
    public Class<TodoEntity> getClazz() {
	return TodoEntity.class;
    }

}
