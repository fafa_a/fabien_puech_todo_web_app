package fr.afpa.cda.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.constants.Constants;
import fr.afpa.cda.controller.command.ICommand;
import fr.afpa.cda.controller.command.UnknownClassCommand;

@WebServlet(urlPatterns = "/")
public class FrontController extends HttpServlet {
  private static final long serialVersionUID = 1L;

  public FrontController() {
    super();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    doProcess(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    doProcess(request, response);
  }

  protected void doProcess(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    ICommand commandInstance = null;
    try {
      commandInstance = (ICommand) getCorrespondingClass(request).newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      e.printStackTrace();
    }

    String nextStep = commandInstance.execute(request, response);

    if (nextStep.startsWith("redirect")) {
      response.sendRedirect(request.getContextPath() + "/do?action=" + nextStep.replace("redirect:", ""));
    } else {
      request.getRequestDispatcher(Constants.JSP_ROOT + nextStep + ".jsp").forward(request, response);
    }

  }

  private Class<?> getCorrespondingClass(HttpServletRequest request) {
    String action = request.getParameter("action");
    try {
      return Class.forName(Constants.CMD_PACKAGE + Constants.CMD_PREFIX + action);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      return UnknownClassCommand.class;
    }
  }

}
