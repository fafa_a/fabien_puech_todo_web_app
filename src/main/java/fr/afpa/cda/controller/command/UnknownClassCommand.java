package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UnknownClassCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
	return "error404";
    }

}
