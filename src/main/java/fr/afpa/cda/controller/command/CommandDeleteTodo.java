package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.services.ITodoService;
import fr.afpa.cda.services.impl.TodoServiceImpl;

public class CommandDeleteTodo implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    Integer id = Integer.parseInt(request.getParameter("id"));
    ITodoService todoService = new TodoServiceImpl();
    todoService.findTodoById(id);

    todoService.deleteTodo(id);
    return "redirect:ListTodo";
  }

}
