package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.services.impl.TodoServiceImpl;

public class CommandCreateTodo implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    TodoServiceImpl todoService = new TodoServiceImpl();
    HttpSession session = request.getSession(true);
    String title = request.getParameter("title");
    String description = request.getParameter("description");
    String username = session.getAttribute("username").toString();

    boolean isDone = Boolean.parseBoolean(request.getParameter("isDone"));

    TodoBean newTodo = new TodoBean(title, description, isDone, username);
    todoService.createTodo(newTodo);

    title = "";
    description = "";
    return "redirect:ListTodo";
  }

}
