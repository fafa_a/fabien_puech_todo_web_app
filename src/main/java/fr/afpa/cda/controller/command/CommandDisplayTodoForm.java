package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.services.ITodoService;
import fr.afpa.cda.services.impl.TodoServiceImpl;

public class CommandDisplayTodoForm implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {

    if (request.getParameter("id") != null) {
      Integer id = Integer.parseInt(request.getParameter("id"));
      ITodoService todoService = new TodoServiceImpl();
      TodoBean todo = todoService.findTodoById(id);
      request.setAttribute("todo", todo);

    }
    return "todo_form";
  }

}
