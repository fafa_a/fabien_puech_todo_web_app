package fr.afpa.cda.controller.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.services.impl.TodoServiceImpl;

public class CommandListTodo implements ICommand {
  TodoServiceImpl todoService = new TodoServiceImpl();

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession(true);
    String username = (String) session.getAttribute("username");
    List<TodoBean> listTodos = todoService.getTodosByUser(username);
    session.setAttribute("listTodos", listTodos);
    return "todo_list";
  }

}
