package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.services.impl.UserServiceImpl;

public class CommandLogin implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession(true);
    String username = (String) session.getAttribute("username");
    System.out.println("username : " + username);
    return "login";
  }

}
