package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.models.UserBean;
import fr.afpa.cda.services.impl.UserServiceImpl;

public class CommandValidateSignup implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String firstname = request.getParameter("firstName");
		String lastname = request.getParameter("lastName");
		String username = request.getParameter("userName");
		String password = request.getParameter("password");
		
		UserBean currentUser = new UserBean(firstname, lastname, username, password);
		
		UserServiceImpl userService = new UserServiceImpl();
		userService.createUser(currentUser);
		
		return "login";
	}

	
}
