package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CommandLogOut implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession(true);
    session.setAttribute("username", "logout");
    return "login";
  }

}
