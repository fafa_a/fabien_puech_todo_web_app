package fr.afpa.cda.controller.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import antlr.Utils;
import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.services.ITodoService;
import fr.afpa.cda.services.impl.TodoServiceImpl;

public class CommandUpdateTodo implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {

    Integer id = Integer.parseInt(request.getParameter("id"));
    ITodoService todoService = new TodoServiceImpl();
    TodoBean todo = todoService.findTodoById(id);
    request.setAttribute("todo", todo);
    HttpSession session = request.getSession(true);
    String title = request.getParameter("title");
    String description = request.getParameter("description");
    String username = session.getAttribute("username").toString();

    boolean isDone = Boolean.parseBoolean(request.getParameter("isDone"));

    TodoBean newTodo = new TodoBean(id, title, description, isDone, username);
    todoService.updateTodo(newTodo);
    return "redirect:ListTodo";
  }

}
