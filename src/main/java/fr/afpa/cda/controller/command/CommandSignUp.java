package fr.afpa.cda.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import fr.afpa.cda.services.impl.UserServiceImpl;

public class CommandSignUp implements ICommand {

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession(true);
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    UserServiceImpl userService = new UserServiceImpl();
    String nextStep = "";
    if (!userService.userExist(username, password)) {
      nextStep = "signup";
    } else {
      session.setAttribute("username", username);
      nextStep = "redirect:ListTodo";
    }
    return nextStep;
  }

}
