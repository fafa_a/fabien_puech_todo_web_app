package fr.afpa.cda.models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_todo")
public class TodoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtodo")
    private Integer idtodo;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "completed")
    private boolean completed = false;

    @Column(name = "username")
    private String username;

    public TodoEntity() {
	super();
    }

    public TodoEntity(String title, String description, boolean completed, String username) {
	super();
	this.title = title;
	this.description = description;
	this.completed = completed;
	this.username = username;
    }

    public TodoEntity(Integer idtodo, String title, String description, boolean completed, String username) {
	super();
	this.idtodo = idtodo;
	this.title = title;
	this.description = description;
	this.completed = completed;
	this.username = username;
    }

    public Integer getIdtodo() {
	return idtodo;
    }

    public void setIdtodo(Integer idtodo) {
	this.idtodo = idtodo;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public boolean isCompleted() {
	return completed;
    }

    public void setCompleted(boolean completed) {
	this.completed = completed;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

}
