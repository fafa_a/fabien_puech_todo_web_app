package fr.afpa.cda.models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iduser")
    private Integer iduser;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    public UserEntity() {
	super();
    }

    public UserEntity(String name, String surname, String username, String password) {
	super();
	this.name = name;
	this.surname = surname;
	this.username = username;
	this.password = password;
    }

    public UserEntity(Integer iduser, String name, String surname, String username, String password) {
	super();
	this.iduser = iduser;
	this.name = name;
	this.surname = surname;
	this.username = username;
	this.password = password;
    }

    public Integer getIduser() {
	return iduser;
    }

    public void setIduser(Integer iduser) {
	this.iduser = iduser;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getSurname() {
	return surname;
    }

    public void setSurname(String surname) {
	this.surname = surname;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

}
