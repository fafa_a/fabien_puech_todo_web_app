package fr.afpa.cda.models.mappers;

import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.cda.models.TodoBean;
import fr.afpa.cda.models.entities.TodoEntity;

public class TodoMapper {

    public static TodoEntity toEntity(TodoBean todoBean) {
	TodoEntity todoEntity = new TodoEntity();
	todoEntity.setIdtodo(todoBean.getIdtodo());
	todoEntity.setTitle(todoBean.getTitle());
	todoEntity.setDescription(todoBean.getDescription());
	todoEntity.setCompleted(todoBean.isCompleted());
	todoEntity.setUsername(todoBean.getUsername());
	return todoEntity;
    }

    public static TodoBean toBean(TodoEntity todoEntity) {
	TodoBean todoBean = new TodoBean();
	todoBean.setIdtodo(todoEntity.getIdtodo());
	todoBean.setTitle(todoEntity.getTitle());
	todoBean.setDescription(todoEntity.getDescription());
	todoBean.setCompleted(todoEntity.isCompleted());
	todoBean.setUsername(todoEntity.getUsername());
	return todoBean;
    }

    public static List<TodoEntity> toEntityList(List<TodoBean> beanList) {
	return beanList.stream().map(b -> toEntity(b)).collect(Collectors.toList());
    }

    public static List<TodoBean> toBeanList(List<TodoEntity> entityList) {
	return entityList.stream().map(TodoMapper::toBean).collect(Collectors.toList());
    }

}
