package fr.afpa.cda.models.mappers;

import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.cda.models.UserBean;
import fr.afpa.cda.models.entities.UserEntity;

public class UserMapper {

    public static UserEntity toEntity(UserBean userBean) {
	UserEntity userEntity = new UserEntity();
	userEntity.setIduser(userBean.getIduser());
	userEntity.setName(userBean.getName());
	userEntity.setSurname(userBean.getSurname());
	userEntity.setUsername(userBean.getUsername());
	userEntity.setPassword(userBean.getPassword());

	return userEntity;
    }

    public static UserBean toBean(UserEntity userEntity) {
	UserBean userBean = new UserBean();
	userBean.setIduser(userEntity.getIduser());
	userBean.setName(userEntity.getName());
	userBean.setSurname(userEntity.getSurname());
	userBean.setUsername(userEntity.getUsername());
	userBean.setPassword(userEntity.getPassword());

	return userBean;
    }

    public static List<UserEntity> toEntityList(List<UserBean> beanList) {
	return beanList.stream().map(b -> toEntity(b)).collect(Collectors.toList());
    }

    public static List<UserBean> toBeanList(List<UserEntity> entityList) {
	return entityList.stream().map(UserMapper::toBean).collect(Collectors.toList());
    }

}
