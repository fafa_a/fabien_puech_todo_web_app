package fr.afpa.cda.models;

public class TodoBean {

    private Integer idtodo;
    private String title;
    private String description;
    private boolean completed = false;
    private String username;

    public TodoBean() {
	super();
    }

    public TodoBean(String title, String description, boolean completed, String username) {
	super();
	this.title = title;
	this.description = description;
	this.completed = completed;
	this.username = username;
    }

    public TodoBean(Integer idtodo, String title, String description, boolean completed, String username) {
	super();
	this.idtodo = idtodo;
	this.title = title;
	this.description = description;
	this.completed = completed;
	this.username = username;
    }

    public Integer getIdtodo() {
	return idtodo;
    }

    public void setIdtodo(Integer idtodo) {
	this.idtodo = idtodo;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public boolean isCompleted() {
	return completed;
    }

    public void setCompleted(boolean completed) {
	this.completed = completed;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    @Override
    public String toString() {
	return "TodoBean [idtodo=" + idtodo + ", title=" + title + ", description=" + description + ", completed="
		+ completed + ", username=" + username + "]";
    }

}
