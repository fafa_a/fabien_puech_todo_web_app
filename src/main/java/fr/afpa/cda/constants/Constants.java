package fr.afpa.cda.constants;

public class Constants {

    public static final String CMD_PACKAGE = "fr.afpa.cda.controller.command.";
    public static final String CMD_PREFIX = "Command";

    public static final String JSP_ROOT = "/views/";

}
