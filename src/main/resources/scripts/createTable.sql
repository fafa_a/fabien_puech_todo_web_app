DROP TABLE IF EXISTS t_user CASCADE;
DROP TABLE IF EXISTS t_todo CASCADE;

CREATE TABLE t_user (
   idUser INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
   name VARCHAR(30) NOT NULL,
   surname VARCHAR(30) NOT NULL,
   username VARCHAR(30) NOT NULL,
   password VARCHAR(30) NOT NULL,
   CONSTRAINT unique_username UNIQUE (username)
);

CREATE TABLE t_todo (
   idTodo INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
   title VARCHAR(50) NOT NULL,
   description VARCHAR NULL,
   completed BOOLEAN NOT NULL,
   username VARCHAR(20) NOT NULL
);

INSERT INTO t_user ( name, surname, username, password)
	VALUES ('cyvian', 'him', 'cycy', 'aze' );
INSERT INTO T_USER (name, surname, username, password)
   VALUES ('bracquemond', 'marie', 'siam', 'aze' );
INSERT INTO T_USER (name, surname, username, password)
   VALUES ('zingraff', 'brice', 'brie', 'aze' );

INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('collation','manger des graines', false, 'cycy' );
INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('collation','boire un thé', false, 'cycy' );
INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('fan girl','regarder un match de rugby', true, 'cycy' );

INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('collation', 'boire une biere', true, 'siam' );
INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('detente', 'regarder des photos de chat', true, 'siam' );
INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('detente', 'faire une sieste', true, 'siam' );

INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('détente', 'jouer à elden ring', true, 'brie' );
INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('collation', 'manger', true, 'brie' );
INSERT INTO T_TODO (title, description, completed, username)
   VALUES ('collation', 'remanger', true, 'brie' );

